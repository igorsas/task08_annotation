package com.igor;

import com.igor.InvokeDifferentMethods.Application;
import com.igor.MyAnnotation.Reflection;
import com.igor.infoAboutUnknownObject.UnknowObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Menu {
    public static void main(String[] args) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            System.out.println("Choose program for calling: " +
                    "\n1 - Unknown object" +
                    "\n2 - Invoke different methods" +
                    "\n3 - Reflection my Annotation" +
                    "\nq - Quit");
            String point = "q";
            try {
                point = reader.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            switch (point) {
                case "1":
                    UnknowObject.main(new String[]{"Test"});
                    break;
                case "2":
                    Application.main(new String[]{"Test"});
                    break;
                case "3":
                    Reflection.main(new String[]{"Test"});
                    break;
                default:
                    return;
            }
        }
    }
}
