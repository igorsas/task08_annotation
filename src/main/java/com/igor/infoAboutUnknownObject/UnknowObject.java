package com.igor.infoAboutUnknownObject;

import com.igor.MyAnnotation.City;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

public class UnknowObject {
    public static void main(String[] args) {
        City city = new City("Lviv", 700000);
        try {
            showInfo(city);
        } catch (InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public static void showInfo(Object object) throws InvocationTargetException, IllegalAccessException {
        Class clazz = object.getClass();
        Field[] allFields = clazz.getDeclaredFields();
        Method[] allMethods = clazz.getMethods();
        Constructor[] allConstructors = clazz.getConstructors();

        System.out.println("Name of class: " + clazz.getName());
        System.out.println("\nAll fields in your class: ");
        for (Field field : allFields) {
            System.out.println(" " + field.getName());
        }

        System.out.println("\n\nAll constructors in your class: ");
        for (Constructor constructor : allConstructors) {
            System.out.println(constructor.getName());
        }

        System.out.println("\n\nAll methods in your class: ");
        for (Method method : allMethods) {
            System.out.println("\nName: " + method.getName());
            System.out.println("Count of parameters: " + method.getParameterCount());
            System.out.println("Type of parameters: " + Arrays.toString(method.getParameterTypes()));
            System.out.println("Return type: " + method.getReturnType());
            System.out.println("Declared annotations: " + Arrays.toString(method.getDeclaredAnnotations()));
        }

        Method[] declaredMethods = clazz.getDeclaredMethods();
        System.out.println("\n\nInfo by declared methods from your object of class:");
        for (Method method : declaredMethods) {
            if(method.getParameterCount() == 0) {
                System.out.println("\nName: " + method.getName());
                method.setAccessible(true);
                System.out.println(method.invoke(object));
            }
        }
    }
}
