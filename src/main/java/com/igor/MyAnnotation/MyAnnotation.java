package com.igor.MyAnnotation;

import java.lang.annotation.*;

@Target({ElementType.METHOD, ElementType.PACKAGE, ElementType.LOCAL_VARIABLE, ElementType.ANNOTATION_TYPE, ElementType.FIELD})
@Inherited
@Retention(RetentionPolicy.RUNTIME)
public @interface MyAnnotation {
    String city() default "Lviv";
    int population() default 700000;
}