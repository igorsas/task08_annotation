package com.igor.MyAnnotation;

import com.igor.MyAnnotation.MyAnnotation;

import java.lang.reflect.Field;

public class Reflection {
    public static void main(String[] args) {
        City city = new City("Barcelona", 1500000);
        Class clazz = city.getClass();
        Field[] declaredFields = clazz.getDeclaredFields();
        for(Field field: declaredFields){
            System.out.println(field.getType() + " " + field.getName());
            MyAnnotation annotation = field.getAnnotation(MyAnnotation.class);
            if (annotation != null)
                System.out.print("City in annotation: " + annotation.city() + ", Population in annotation: " + annotation.population() + "\n\n");
        }
    }
}
