package com.igor.MyAnnotation;

@FunctionalInterface
public interface Printable {
    void print();
}
