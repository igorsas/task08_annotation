package com.igor.MyAnnotation;

import java.util.Objects;

public class City {
    @MyAnnotation(city = "Barcelona")
    private String name;

    @MyAnnotation(population = 1500000)
    private int countPeople;

    public City(String name, int countPeople) {
        this.name = name;
        this.countPeople = countPeople;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof City)) return false;
        City city = (City) o;
        return getCountPeople() == city.getCountPeople() &&
                Objects.equals(getName(), city.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getCountPeople());
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "City{" +
                "name='" + name + '\'' +
                ", countPeople=" + countPeople +
                '}';
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCountPeople() {
        return countPeople;
    }

    public void setCountPeople(int countPeople) {
        this.countPeople = countPeople;
    }

}
