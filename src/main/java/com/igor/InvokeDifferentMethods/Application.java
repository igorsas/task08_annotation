package com.igor.InvokeDifferentMethods;

import com.igor.MyAnnotation.City;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.Scanner;

public class Application {
    public static void main(String[] args) {
        ClassForWorkWithStringMethod object = new ClassForWorkWithStringMethod();
        System.out.println("Invoke method with different parameters");
        callMyMethod(object);
        System.out.println("Set value in ");
        try {
            setValueIntoField(new City("Kyiv", 200000));
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public static void callMyMethod(Object object){
        Class clazz = object.getClass();
        try {
            Method method = clazz.getDeclaredMethod("myMethodString");
            method.invoke(object);
        } catch (IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            e.printStackTrace();
        }

        try {
            String[] strings = new String[]{"First test", "Second test", "Third test"};
            Method method = clazz.getDeclaredMethod("myMethodString", String.class, String[].class);
            method.setAccessible(true);
            String[] calledMethods = (String[]) method.invoke(object, "There is method with arguments", strings);
            for (String str : calledMethods){
                System.out.println(str);
            }
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    public static void setValueIntoField(Object object) throws IllegalAccessException {
        Scanner scanner = new Scanner(System.in);
        Class clazz = object.getClass();
        Field[] allFields = clazz.getDeclaredFields();
        System.out.println("\nAll fields in your class: ");
        for (Field field : allFields) {
            System.out.println(" " + field.getName());
            System.out.println("Input data in " + field.getType() + " type");
            Type type = field.getType();
            field.setAccessible(true);
            System.out.println(((Class) type).getName());
            if(((Class) type).isPrimitive()) {
                field.set(object, scanner.nextInt());
            }else{
                field.set(object, scanner.nextLine());
            }
            System.out.println("Field after changing: " + field.get(object) + "\n");
        }


    }

}
