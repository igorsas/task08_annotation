package com.igor.InvokeDifferentMethods;

public class ClassForWorkWithStringMethod {


    public String[] myMethodString(String str, String... args) {
        String[] stringArray = new String[args.length];
        for (int i = 0; i < args.length; i++) {
            stringArray[i] = str + " --> " + args[i] + " --> " + i;
        }
        return stringArray;
    }

    public void myMethodString() {
        System.out.println("There's empty method");
    }
}
